package com.atlassian.confluence.ext.code.descriptor;


import java.util.List;

/**
 * Interface for objects with implement the actual Confluence version strategies
 * as they differ (in an API incompatible way) between versions of Confluence.
 */
public interface ConfluenceStrategy {

    /**
     * List all built-in brushes.
     *
     * @return The locations of the built-in brushes
     */
    BrushDefinition[] listBuiltinBrushes();

    /**
     * List all built-in themes.
     *
     * @return The locations of the built-in themes
     */
    ThemeDefinition[] listBuiltinThemes();

    /**
     * List all localization supported in the plugin
     *
     * @return The language key of the localization
     */
    List<String> listLocalization();
}
