package com.atlassian.confluence.ext.code.languages;

/**
 * Thrown in case of problems with the parsing of the language file.
 */
public final class InvalidLanguageException extends Exception {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 1L;

    private String errorMsgKey;
    private Object[] params;

    public InvalidLanguageException(final String errorMessage, Throwable cause) {
        super(errorMessage, cause);
        this.errorMsgKey = errorMessage;
    }

    /**
     * Default constructor.
     *
     * @param errorMsgKey The message key
     * @param params      The parameters of the message
     */
    public InvalidLanguageException(final String errorMsgKey,
                                    final Object... params) {
        super();
        this.errorMsgKey = errorMsgKey;
        this.params = params;
    }

    /**
     * @return the errorMsgKey
     */
    public String getErrorMsgKey() {
        return errorMsgKey;
    }

    /**
     * @return the params
     */
    public Object[] getParams() {
        return params;
    }

}
