package com.atlassian.confluence.ext.code.languages.impl;

import com.atlassian.confluence.ext.code.languages.DuplicateLanguageException;
import com.atlassian.confluence.ext.code.languages.Language;
import com.atlassian.confluence.ext.code.languages.LanguageRegistry;
import com.atlassian.confluence.ext.code.languages.UnknownLanguageException;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the language registry.
 */
@Component
public final class LanguageRegistryImpl implements LanguageRegistry {

    /**
     * Hash table of all languages as registered by alias. Multi-thread safe.
     */
    private Map<String, Language> languages = new Hashtable<>();
    private Map<String, Language> languagesByName = new HashMap<>();

    public LanguageRegistryImpl() {
    }

    /**
     * {@inheritDoc}
     */
    public boolean isLanguageRegistered(final String alias) {
        return languages.containsKey(alias);
    }

    public Language getLanguage(String name) throws UnknownLanguageException {
        Language language = languagesByName.get(name);
        if (language == null)
            language = languages.get(name);

        if (language == null)
            throw new UnknownLanguageException(name);

        return language;
    }

    /**
     * {@inheritDoc}
     */
    public String getWebResourceForLanguage(final String alias)
            throws UnknownLanguageException {
        Language lang = languages.get(alias);
        if (lang == null) {
            throw new UnknownLanguageException(alias);
        }
        return lang.getWebResource();
    }


    /**
     * {@inheritDoc}
     */
    public List<Language> listLanguages() {
        return Lists.newArrayList(languagesByName.values());
    }

    public void addLanguage(final Language language) throws DuplicateLanguageException {
        for (Language lang : listLanguages()) {
            if (lang.getName().equals(language.getName())) {
                throw new DuplicateLanguageException(
                        "newcode.language.register.duplicate.name", language
                        .getName());
            }
        }

        for (String alias : language.getAliases()) {
            if (isLanguageRegistered(alias)) {
                throw new DuplicateLanguageException(
                        "newcode.language.register.duplicate.alias", alias);
            }
        }

        for (String alias : language.getAliases()) {
            languages.put(alias, language);
        }
        languagesByName.put(language.getName(), language);
    }

    /**
     * {@inheritDoc}
     */
    public void unregisterLanguage(final String name) {
        languages.entrySet().removeIf(entry -> entry.getValue().getName().equals(name)
                && !entry.getValue().isBuiltIn());
        languagesByName.remove(name);
    }
}
