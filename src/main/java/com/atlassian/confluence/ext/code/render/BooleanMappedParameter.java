package com.atlassian.confluence.ext.code.render;

import java.util.Map;

/**
 * Implementation of a boolean parameter for which a mapping has to be made
 * between the name used by the macro and the syntax highlighter library.
 */
public final class BooleanMappedParameter extends AbstractBooleanMappedParameter {

    /**
     * Default constructor.
     *
     * @param name       The name used by the macro
     * @param mappedName The name used by the syntax highlighter
     */
    public BooleanMappedParameter(final String name, final String mappedName) {
        super(name, mappedName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getValue(final Map<String, String> parameters)
            throws InvalidValueException {
        return internalGetValue(parameters);
    }

}
