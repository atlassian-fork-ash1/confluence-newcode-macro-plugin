package com.atlassian.confluence.ext.code.render;

import java.util.Map;

/**
 * Implementation of a integer parameter for which a mapping has to be made
 * between the name used by the macro and the syntax highlighter library.
 */
public final class IntegerMappedParameter extends MappedParameter {

    /**
     * Default constructor.
     *
     * @param name       The name used by the macro
     * @param mappedName The name used by the syntax highlighter
     */
    public IntegerMappedParameter(final String name, final String mappedName) {
        super(name, mappedName);
    }

    /**
     * @see com.atlassian.confluence.ext.code.render.MappedParameter#getValue(java.util.Map)
     */
    @Override
    public String getValue(final Map<String, String> parameters)
            throws InvalidValueException {
        String retval = super.getValue(parameters);
        try {
            if (retval != null) {
                Integer.parseInt(retval);
            }
        } catch (NumberFormatException e) {
            throw new InvalidValueException(this.getMacroName());
        }
        return retval;
    }
}
