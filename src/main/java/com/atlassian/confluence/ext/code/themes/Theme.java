package com.atlassian.confluence.ext.code.themes;

import java.util.Map;

/**
 * Interface for the registration of supported languages.
 */
public interface Theme {

    /**
     * Returns the name of the theme.
     *
     * @return The name of the name
     */
    String getName();

    /**
     * Returns the URL to the stylesheet.
     *
     * @return The URL to the stylehseet
     */
    String getStyleSheetUrl();

    /**
     * Returns whether this theme is built-in.
     *
     * @return whether this theme is built-in
     */
    boolean isBuiltIn();

    /**
     * @return the id of the web resource to include for this theme
     */
    String getWebResource();

    /**
     * @return the properties for the panel component which describe the default layout
     */
    Map<String, String> getDefaultLayout();
}
