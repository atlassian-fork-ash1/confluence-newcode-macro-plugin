package com.atlassian.confluence.ext.code.util;

/**
 * Constants for the new code macro
 */
public final class Constants {

    /**
     * The unique key for this plugin.
     */
    public static final String PLUGIN_KEY = "com.atlassian.confluence.ext.newcode-macro-plugin";

    /**
     * Private constructor.
     */
    private Constants() {
        super();
    }
}
