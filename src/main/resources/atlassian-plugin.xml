<?xml version="1.0" encoding="UTF-8"?>
<atlassian-plugin key="${atlassian.plugin.key}" name="${atlassian.plugin.name}" plugins-version="2">
    <plugin-info>
        <description>Macro and default formatters for pretty-printing source code in a wiki page.</description>
        <vendor name="Atlassian, Inc." url="http://www.atlassian.com"/>
        <version>${atlassian.plugin.version}</version>
        <param name="configure.url">/admin/plugins/newcode/configure.action</param>
        <param name="atlassian-data-center-compatible">true</param>
    </plugin-info>
    <!--
        Main macro description.
    -->

    <!-- The macro deliberately has no metadata. It exists to allow legacy usages of newcode to continue to execute
         but should not be available in the Macro Browser or via auto-complete for future use -->
    <xhtml-macro key="newcode-xhtml" name="newcode" class="com.atlassian.confluence.ext.code.NewCodeMacro"
                 icon="/download/resources/com.atlassian.confluence.ext.newcode-macro-plugin:macro-icons/macro-icon.png">
        <description key="com.atlassian.confluence.ext.newcode-macro-plugin.code.desc" />

        <resource type="velocity" name="help" location="templates/macros/newcode/newcodemacro-help.vm">
            <param name="help-section" value="advanced"/>
        </resource>

        <!-- Images for the notation guide -->
        <resource type="download" name="collapse.png" location="templates/macros/newcode/collapse.png">
            <param name="content-type" value="image/png"/>
        </resource>

        <resource type="download" name="controls.png" location="templates/macros/newcode/controls.png">
            <param name="content-type" value="image/png"/>
        </resource>

        <resource type="download" name="default.png" location="templates/macros/newcode/default.png">
            <param name="content-type" value="image/png"/>
        </resource>

        <resource type="download" name="django.png" location="templates/macros/newcode/django.png">
            <param name="content-type" value="image/png"/>
        </resource>

        <resource type="download" name="firstline.png" location="templates/macros/newcode/firstline.png">
            <param name="content-type" value="image/png"/>
        </resource>

        <resource type="download" name="linenumbers.png" location="templates/macros/newcode/linenumbers.png">
            <param name="content-type" value="image/png"/>
        </resource>

        <resource type="download" name="ruler.png" location="templates/macros/newcode/ruler.png">
            <param name="content-type" value="image/png"/>
        </resource>

        <resource type="download" name="title.png" location="templates/macros/newcode/title.png">
            <param name="content-type" value="image/png"/>
        </resource>

        <resource type="download" name="vbnet.png" location="templates/macros/newcode/vbnet.png">
            <param name="content-type" value="image/png"/>
        </resource>
        <resource type="i18n" name="i18n" location="com/atlassian/confluence/ext/code/newcode"/>
        <device-type>mobile</device-type>
    </xhtml-macro>

    <macro-migrator key="newcode-migration"
                    macro-name="newcode"
                    class="com.atlassian.confluence.ext.code.NewCodeMacroMigration"/>

    <!-- Set new code macro state to "enabled" because Confluence 4 is bundled with old code macro. Else integration tests will fail.
         Need to set back to "disabled" when newcode macro is bundled in -->
    <macro name="newcode" class="com.atlassian.confluence.ext.code.NewCodeMacro" key="newcode" state="enabled"
           icon="/download/resources/com.atlassian.confluence.ext.newcode-macro-plugin:macro-icons/macro-icon.png">
        <description key="com.atlassian.confluence.ext.newcode-macro-plugin.code.desc" />
        <parameters/>
    </macro>

    <xhtml-macro key="code-xhtml" name="code" class="com.atlassian.confluence.ext.code.NewCodeMacro"
                 icon="/download/resources/com.atlassian.confluence.ext.newcode-macro-plugin:macro-icons/macro-icon.png">
        <description key="com.atlassian.confluence.ext.newcode-macro-plugin.code.desc" />

        <category name="formatting"/>

        <resource type="velocity" name="help" location="templates/macros/newcode/newcodemacro-help.vm">
            <param name="help-section" value="advanced"/>
        </resource>

        <!-- Support for the macro browser -->
        <parameters>
            <parameter name="language" type="string"/>
            <parameter name="title" type="string"/>
            <parameter name="collapse" type="boolean"/>
            <parameter name="linenumbers" type="boolean"/>
            <parameter name="firstline" type="string"/>
            <parameter name="theme" type="enum" default="Default">
                <value name="Default"/>
                <value name="DJango"/>
                <value name="Emacs"/>
                <value name="FadeToGrey"/>
                <value name="Midnight"/>
                <value name="RDark"/>
                <value name="Eclipse"/>
                <value name="Confluence"/>
            </parameter>
        </parameters>

        <resource type="i18n" name="i18n" location="com/atlassian/confluence/ext/code/newcode"/>
        <device-type>mobile</device-type>
    </xhtml-macro>

    <macro name="code" class="com.atlassian.confluence.ext.code.NewCodeMacro" key="code" state="enabled">
        <description key="com.atlassian.confluence.ext.newcode-macro-plugin.code.desc" />
        <parameters/>
    </macro>

    <!-- 
     	Configuration UI for the plugin     
    -->
    <resource type="i18n" name="i18n" location="com/atlassian/confluence/ext/code/newcode"/>

    <web-resource key="code-macro-editor" name="Code Macro Editor Resources">
        <resource type="download" name="code-macro-languages.js" location="/scripts/code-macro-languages.js"/>

        <dependency>confluence.editor.actions:editor-macro-browser</dependency>
        <dependency>confluence.web.resources:quicksearchdropdown</dependency>
        <context>macro-browser</context>
    </web-resource>

    <web-item key="newcode-admin-link"
              name="Configure Code Macro Admin Console Link"
              section="system.admin/configuration"
              weight="201">
        <description>Adds the Code Macro configuration action to the Confluence administration console</description>
        <label key="newcode.config.macro"/>
        <link>/admin/plugins/newcode/configure.action</link>
    </web-item>

    <xwork name="Code Macro JSON Actions" key="newcode-json-actions">
        <description>XWork/WebWork endpoints for returning JSON to the client browser.</description>
        <package name="newcode-json" extends="default" namespace="/plugins/newcode">
            <default-interceptor-ref name="defaultStack"/>

            <action name="getlanguages" class="com.atlassian.confluence.ext.code.actions.GetLanguagesAction">
                <result name="success" type="json"/>
            </action>
        </package>
    </xwork>

    <xwork name="Configure Code Macro" key="configure-newcode">
        <package name="newcode" extends="default" namespace="/admin/plugins/newcode">
            <default-interceptor-ref name="validatingStack"/>
            <action name="configure" class="com.atlassian.confluence.ext.code.config.ConfigureNewcodeAction"
                    method="input">
                <result name="input" type="velocity">/templates/macros/newcode/config/configure-newcode.vm</result>
            </action>

            <action name="save" class="com.atlassian.confluence.ext.code.config.ConfigureNewcodeAction" method="save">
                <param name="RequireSecurityToken">true</param>
                <result name="input" type="velocity">/templates/macros/newcode/config/configure-newcode.vm</result>
                <result name="error" type="velocity">/templates/macros/newcode/config/configure-newcode.vm</result>
                <result name="success" type="velocity">/templates/macros/newcode/config/configure-newcode.vm</result>
            </action>

            <action name="addlanguage" class="com.atlassian.confluence.ext.code.config.ConfigureNewcodeAction"
                    method="addLanguage">
                <param name="RequireSecurityToken">true</param>
                <result name="input" type="velocity">/templates/macros/newcode/config/configure-newcode.vm</result>
                <result name="error" type="velocity">/templates/macros/newcode/config/configure-newcode.vm</result>
                <result name="success" type="velocity">/templates/macros/newcode/config/configure-newcode.vm</result>
            </action>

            <action name="removelanguage" class="com.atlassian.confluence.ext.code.config.ConfigureRpcAction"
                    method="removeLanguage">
                <param name="RequireSecurityToken">true</param>
                <result name="success" type="rawText"/>
                <result name="input" type="rawText"/>
                <result name="error" type="rawText"/>
            </action>
        </package>
    </xwork>

    <web-resource key="newcode-admin" name="Admin JavaScript &amp; CSS">

        <resource type="download" name="code-macro-admin.js" location="scripts/code-macro-admin.js"/>
        <resource type="download" name="code-macro-admin.css" location="styles/code-macro-admin.css"/>
        <resource type="download" name="newcode-templates.soy.js" location="soy/newcode-templates.soy"/>

        <transformation extension="soy">
            <transformer key="soyTransformer">
                <functions>com.atlassian.confluence.plugins.soy:soy-core-functions</functions>
            </transformer>
        </transformation>
        <transformation extension="js">
            <transformer key="jsI18n"/>
        </transformation>

        <dependency>confluence.web.resources:ajs</dependency>
    </web-resource>

    <!-- 
    	Core resources for the Newcode macro.
    -->
    <web-resource key="syntaxhighlighter" name="SyntaxHighlighter">
        <!-- The core library files -->
        <resource type="download" name="shCore.js" location="sh/scripts/shCore.js"/>
        <resource type="download" name="shLegacy.js" location="sh/scripts/shLegacy.js"/>
        <resource type="download" name="collapseSource.js" location="sh/scripts/collapseSource.js"/>

        <transformation extension="js">
            <transformer key="jsI18n"/>
        </transformation>

        <context>code-macro</context>
    </web-resource>

    <web-resource key="syntaxhighlighter-init" name="SyntaxHighlighter Init">
        <!-- This script is used to bootstrap the SyntaxHighlighter when using Confluence -->
        <resource type="download" name="asyncLoader.js" location="sh/asyncLoader.js"/>

        <!-- The core CSS stylesheets -->
        <resource type="download" name="shCore.css" location="sh/styles/shCore.css"/>
        <resource type="download" name="collapseSource.css" location="sh/styles/collapseSource.css"/>

        <transformation extension="js">
            <transformer key="jsI18n"/>
        </transformation>

        <context>viewcontent</context>
        <context>preview</context>
    </web-resource>

    <!-- Resources for Confluence mobile -->
    <web-resource key="syntaxhighlighter-mobile" name="SyntaxHighlighter Mobile">
        <resource type="download" name="shCore.css" location="sh/styles/shCore.css"/>
        <resource type="download" name="shCoreMobile.css" location="sh/styles/shCoreMobile.css"/>
        <context>confluence.macros.newcode.macro.mobile</context>
    </web-resource>

    <web-resource key="syntaxhighlighter-export" name="Syntax Highlighter Stylesheet for export">
        <resource type="download" name="shExport.css" location="sh/shExport.css"/>
    </web-resource>

    <!--
      All brushes supported by the SyntaxHighlighter
  -->
    <web-resource key="syntaxhighlighter-brushes" name="SyntaxHighlighter language definitions">
        <resource type="download" name="shBrushAppleScript.js" location="sh/scripts/shBrushAppleScript.js"/>
        <resource type="download" name="shBrushAS3.js" location="sh/scripts/shBrushAS3.js"/>
        <resource type="download" name="shBrushBash.js" location="sh/scripts/shBrushBash.js"/>
        <resource type="download" name="shBrushColdFusion.js" location="sh/scripts/shBrushColdFusion.js"/>
        <resource type="download" name="shBrushCpp.js" location="sh/scripts/shBrushCpp.js"/>
        <resource type="download" name="shBrushCSharp.js" location="sh/scripts/shBrushCSharp.js"/>
        <resource type="download" name="shBrushCss.js" location="sh/scripts/shBrushCss.js"/>
        <resource type="download" name="shBrushDelphi.js" location="sh/scripts/shBrushDelphi.js"/>
        <resource type="download" name="shBrushDiff.js" location="sh/scripts/shBrushDiff.js"/>
        <resource type="download" name="shBrushErlang.js" location="sh/scripts/shBrushErlang.js"/>
        <resource type="download" name="shBrushGroovy.js" location="sh/scripts/shBrushGroovy.js"/>
        <resource type="download" name="shBrushJava.js" location="sh/scripts/shBrushJava.js"/>
        <resource type="download" name="shBrushJavaFX.js" location="sh/scripts/shBrushJavaFX.js"/>
        <resource type="download" name="shBrushJScript.js" location="sh/scripts/shBrushJScript.js"/>
        <resource type="download" name="shBrushPerl.js" location="sh/scripts/shBrushPerl.js"/>
        <resource type="download" name="shBrushPhp.js" location="sh/scripts/shBrushPhp.js"/>
        <resource type="download" name="shBrushPlain.js" location="sh/scripts/shBrushPlain.js"/>
        <resource type="download" name="shBrushPowerShell.js" location="sh/scripts/shBrushPowerShell.js"/>
        <resource type="download" name="shBrushPython.js" location="sh/scripts/shBrushPython.js"/>
        <resource type="download" name="shBrushRuby.js" location="sh/scripts/shBrushRuby.js"/>
        <resource type="download" name="shBrushSass.js" location="sh/scripts/shBrushSass.js"/>
        <resource type="download" name="shBrushScala.js" location="sh/scripts/shBrushScala.js"/>
        <resource type="download" name="shBrushSql.js" location="sh/scripts/shBrushSql.js"/>
        <resource type="download" name="shBrushVb.js" location="sh/scripts/shBrushVb.js"/>
        <resource type="download" name="shBrushXml.js" location="sh/scripts/shBrushXml.js"/>
        <resource type="download" name="shBrushYaml.js" location="sh/scripts/shBrushYaml.js"/>

        <!-- Add custom brushes here -->

        <!-- End custom brushes -->

        <context>code-macro</context>
    </web-resource>

    <!--
        Resources for the Default theme.
    -->
    <web-resource key="sh-theme-default" name="SyntaxHighlighter Default Theme">
        <resource type="download" name="shThemeDefault.css" location="sh/styles/shThemeDefault.css"/>
        <!-- The values below are used by the plugin to determine the default panel layout -->
        <param name="layout-borderWidth" value="1px"/>
    </web-resource>

    <!--
        Resources for the Django theme.
    -->
    <web-resource key="sh-theme-django" name="SyntaxHighlighter Django Theme">
        <resource type="download" name="shThemeDjango.css" location="sh/styles/shThemeDjango.css"/>
        <!-- The values below are used by the plugin to determine the default panel layout -->
        <param name="layout-borderColor" value="#497958"/>
        <param name="layout-borderStyle" value="dashed"/>
        <param name="layout-titleBGColor" value="#497958"/>
        <param name="layout-borderWidth" value="1px"/>
    </web-resource>

    <!--
        Resources for the Emacs theme.
    -->
    <web-resource key="sh-theme-emacs" name="SyntaxHighlighter Emacs Theme">
        <resource type="download" name="shThemeEmacs.css" location="sh/styles/shThemeEmacs.css"/>
        <!-- The values below are used by the plugin to determine the default panel layout -->
        <param name="layout-borderColor" value="grey"/>
        <param name="layout-borderStyle" value="dashed"/>
        <param name="layout-titleBGColor" value="lightGrey"/>
        <param name="layout-borderWidth" value="1px"/>
    </web-resource>

    <!--
        Resources for the FadeToGrey theme.
    -->
    <web-resource key="sh-theme-fadetogrey" name="SyntaxHighlighter Fade To Grey Theme">
        <resource type="download" name="shThemeFadeToGrey.css" location="sh/styles/shThemeFadeToGrey.css"/>
        <!-- The values below are used by the plugin to determine the default panel layout -->
        <param name="layout-borderColor" value="grey"/>
        <param name="layout-borderStyle" value="dashed"/>
        <param name="layout-titleBGColor" value="lightGrey"/>
        <param name="layout-borderWidth" value="1px"/>
    </web-resource>

    <!--
        Resources for the Midnight theme.
    -->
    <web-resource key="sh-theme-midnight" name="SyntaxHighlighter Midnight Theme">
        <resource type="download" name="shThemeMidnight.css" location="sh/styles/shThemeMidnight.css"/>
        <!-- The values below are used by the plugin to determine the default panel layout -->
        <param name="layout-borderColor" value="grey"/>
        <param name="layout-borderStyle" value="dashed"/>
        <param name="layout-titleBGColor" value="lightGrey"/>
        <param name="layout-borderWidth" value="1px"/>
    </web-resource>

    <!--
        Resources for the RDark theme.
    -->
    <web-resource key="sh-theme-rdark" name="SyntaxHighlighter RDark Theme">
        <resource type="download" name="shThemeRDark.css" location="sh/styles/shThemeRDark.css"/>
        <!-- The values below are used by the plugin to determine the default panel layout -->
        <param name="layout-borderColor" value="grey"/>
        <param name="layout-borderStyle" value="dashed"/>
        <param name="layout-titleBGColor" value="lightGrey"/>
        <param name="layout-borderWidth" value="1px"/>
    </web-resource>

    <!--
        Resources for the Eclipse theme.
    -->
    <web-resource key="sh-theme-eclipse" name="SyntaxHighlighter Eclipse Theme">
        <resource type="download" name="shThemeEclipse.css" location="sh/styles/shThemeEclipse.css"/>
        <!-- The values below are used by the plugin to determine the default panel layout -->
        <param name="layout-borderColor" value="#AFAFAF"/>
        <param name="layout-borderStyle" value="dashed"/>
        <param name="layout-titleBGColor" value="#AFAFAF"/>
        <param name="layout-borderWidth" value="1px"/>
    </web-resource>

    <!--
        Resources for the Confluence theme.
    -->
    <web-resource key="sh-theme-confluence" name="SyntaxHighlighter Confluence Theme">
        <resource type="download" name="shThemeConfluence.css" location="sh/styles/shThemeConfluence.css"/>
        <!-- The values below are used by the plugin to determine the default panel layout -->
        <param name="layout-borderColor" value="grey"/>
        <param name="layout-borderStyle" value="dashed"/>
        <param name="layout-titleBGColor" value="lightGrey"/>
        <param name="layout-borderWidth" value="1px"/>
    </web-resource>

    <web-resource key="macro-icons" name="Code Macro Icons">
        <resource type="download" name="macro-icon.png" location="macro-icon.png"/>
    </web-resource>

    <web-resource key="editor-scripts" name="Code Macro Editor Scripts">
        <resource type="download" name="paste-cleanup.js" location="scripts/paste-cleanup.js"/>
        <context>editor-v3</context>
    </web-resource>

</atlassian-plugin>
