package com.atlassian.confluence.ext.code.descriptor;

import com.atlassian.confluence.ext.code.AbstractNewCodeMacroTest;
import com.atlassian.confluence.ext.code.util.Constants;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfluenceStrategyTest extends AbstractNewCodeMacroTest {
    private static final int EXPECTED_BUILTIN_BRUSHES_COUNT = 26;
    private static final int EXPECTED_BUILTIN_THEMES_COUNT = 8;

    @Mock
    private PluginAccessor pluginAccessor;

    private ConfluenceStrategy strategy;

    @Before
    public void setUp() throws Exception {
        Plugin plugin = createMockPlugin();
        when(pluginAccessor.getPlugin(Constants.PLUGIN_KEY)).thenReturn(plugin);

        strategy = new ConfluenceStrategyImpl(pluginAccessor);
    }

    @Test
    public void testListBuiltInBrushes() throws Exception {
        BrushDefinition[] brushes = strategy.listBuiltinBrushes();
        assertNotNull(brushes);
        assertEquals(EXPECTED_BUILTIN_BRUSHES_COUNT, brushes.length);

        // Spot check
        boolean found = false;
        for (BrushDefinition brush : brushes) {
            if (brush.getLocation().equals("sh/scripts/shBrushCSharp.js")
                    && brush.getWebResourceId().equals(Constants.PLUGIN_KEY +
                    ":" + "syntaxhighlighter-brushes")) {
                found = true;
                break;
            }
        }
        assertTrue(found);
    }

    @Test
    public void testListBuiltInThemes() throws Exception {
        ThemeDefinition[] themes = strategy.listBuiltinThemes();
        assertNotNull(themes);
        assertEquals(EXPECTED_BUILTIN_THEMES_COUNT, themes.length);

        // Spot check
        boolean found = false;
        for (ThemeDefinition theme : themes) {
            if (theme.getLocation().equals("sh/styles/shThemeDjango.css")
                    && theme.getWebResourceId().equals(Constants.PLUGIN_KEY +
                    ":" + "sh-theme-django")) {
                found = true;
                break;
            }
        }
        assertTrue(found);
    }
}
