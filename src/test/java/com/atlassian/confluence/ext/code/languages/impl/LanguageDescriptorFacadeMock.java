package com.atlassian.confluence.ext.code.languages.impl;

import com.atlassian.confluence.ext.code.descriptor.BrushDefinition;
import com.atlassian.confluence.ext.code.descriptor.DescriptorFacade;
import com.atlassian.confluence.ext.code.util.Constants;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;

/**
 * Provides mocking of the descriptor facade to load the built-in languages
 * for testing purposes.
 */
public final class LanguageDescriptorFacadeMock {

    private static final String WEB_RESOURCE = Constants.PLUGIN_KEY + ":syntaxhighlighter-brushes";

    private static final BrushDefinition[] BUILTIN_LANGUAGES = {
        new BrushDefinition("sh/scripts/shBrushAS3.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushBash.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushColdFusion.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushCpp.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushCss.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushDelphi.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushDiff.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushErlang.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushGroovy.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushJava.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushJavaFX.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushJScript.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushPerl.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushPhp.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushPlain.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushPowerShell.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushPython.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushRuby.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushScala.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushSql.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushVb.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushXml.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushYaml.js", WEB_RESOURCE)
    };

    @Mock
    public DescriptorFacade descriptorFacade;

    /**
     *
     */
    public LanguageDescriptorFacadeMock() {
        super();
        MockitoAnnotations.initMocks(this);
        when(descriptorFacade.listBuiltinBrushes()).thenReturn(BUILTIN_LANGUAGES);
    }
}
