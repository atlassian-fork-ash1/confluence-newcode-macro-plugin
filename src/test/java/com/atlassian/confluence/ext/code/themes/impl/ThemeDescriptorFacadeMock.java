package com.atlassian.confluence.ext.code.themes.impl;

import com.atlassian.confluence.ext.code.descriptor.DescriptorFacade;
import com.atlassian.confluence.ext.code.descriptor.ThemeDefinition;
import com.atlassian.confluence.ext.code.util.Constants;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;

/**
 * Provides mocking of the descriptor facade to load the built-in themes
 * for testing purposes.
 */
public final class ThemeDescriptorFacadeMock {

    private static final String WEB_RESOURCE_PREFIX = Constants.PLUGIN_KEY + ":sh-theme-";

    private static final Map<String, String> defaultConfluenceLayout = new HashMap<>();

    private static final ThemeDefinition[] BUILTIN_THEMES = {
            new ThemeDefinition("sh/styles/shThemeDefault.css", WEB_RESOURCE_PREFIX + "default", new HashMap<>()),
            new ThemeDefinition("sh/styles/shThemeDjango.css", WEB_RESOURCE_PREFIX + "django", new HashMap<>()),
            new ThemeDefinition("sh/styles/shThemeEmacs.css", WEB_RESOURCE_PREFIX + "emacs", new HashMap<>()),
            new ThemeDefinition("sh/styles/shThemeFadeToGrey.css", WEB_RESOURCE_PREFIX + "fadetogrey", new HashMap<>()),
            new ThemeDefinition("sh/styles/shThemeMidnight.css", WEB_RESOURCE_PREFIX + "midnight", new HashMap<>()),
            new ThemeDefinition("sh/styles/shThemeRDark.css", WEB_RESOURCE_PREFIX + "rdark", new HashMap<>()),
            new ThemeDefinition("sh/styles/shThemeEclipse.css", WEB_RESOURCE_PREFIX + "eclipse", new HashMap<>()),
            new ThemeDefinition("sh/styles/shThemeConfluence.css", WEB_RESOURCE_PREFIX + "confluence", defaultConfluenceLayout)
    };

    static {
        defaultConfluenceLayout.put("borderColor", "grey");
        defaultConfluenceLayout.put("borderStyle", "dashed");
        defaultConfluenceLayout.put("titleBGColor", "lightGrey");
        defaultConfluenceLayout.put("borderWidth", "1px");
    }

    @Mock
    private DescriptorFacade descriptorFacade;

    /**
     * Default constructor.
     */
    public ThemeDescriptorFacadeMock() {
        super();
        MockitoAnnotations.initMocks(this);
    }

    public DescriptorFacade getMock() {
        when(descriptorFacade.listBuiltinThemes()).thenReturn(BUILTIN_THEMES);
        return descriptorFacade;
    }

}
