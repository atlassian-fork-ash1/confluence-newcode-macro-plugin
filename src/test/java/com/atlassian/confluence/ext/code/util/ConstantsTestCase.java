package com.atlassian.confluence.ext.code.util;

import junit.framework.TestCase;

/**
 * Unit test-case for the {@link Constants}.
 */
public final class ConstantsTestCase extends TestCase {

    /**
     * Test whether the constants are correctly loaded from the properties (i.e.
     * not null).
     */
    public void testConstants() {
        assertNotNull(Constants.PLUGIN_KEY);
    }
}
